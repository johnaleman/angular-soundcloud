(function() {
    'use strict';

    angular
        .module('app.soundcloud')
        .controller('SCController', SCController);

    SCController.$inject = ['$log', '$scope', 'SCFactory'];

    function SCController($log, $scope, SCFactory) {
        var vm = this;

        vm.viewType = 'list';
        vm.artworkUrl = '';
        vm.trackList = [];
        vm.currentPage = 1;
        vm.query = 'tool';
        vm.setViewType = setViewType;
        vm.doSearch = doSearch;
        vm.handleTrackClick = handleTrackClick;
        vm.handleNextPageClick = handleNextPageClick;
        vm.handlePrevPageClick = handlePrevPageClick;

        var storedSearchQuery = angular.copy(vm.query); // to paginate with same query


        var iframe = document.querySelector('#sc-iframe');
        var player = SC.Widget(iframe);

        var options = {
            auto_play: false,
            currentPage: vm.currentPage,
            callback: function() {
                handlePlayerReady();
            }
        };

        // listeners
        player.bind(SC.Widget.Events.READY, handleEventsReady);
        player.bind(SC.Widget.Events.FINISH, handleFinish);

        vm.doSearch(true, true);
        function doSearch(updateStoredQuery, autoLoadTrack) {
            var updateStoredQuery = updateStoredQuery || false;
            var autoLoadTrack = autoLoadTrack || false;

            if(updateStoredQuery === true) {
                storedSearchQuery = angular.copy(vm.query);
                vm.currentPage = 1;
            }

            options['currentPage'] = vm.currentPage;

            var $promise = SCFactory.searchTrack(storedSearchQuery, options);
            $promise.then(function(response) {
                vm.trackList = response.collection;
                vm.nextPageQuery = response.next_href;

                // load initial track
                if(autoLoadTrack === true) {
                    loadTrack(vm.trackList[0], options);
                }

                // clear search
                if(updateStoredQuery === true) {
                    vm.query = '';
                }
            });
        }

        function loadTrack(track, pOptions) {
            var url = track.uri;
            angular.extend(options, pOptions);
            vm.artworkUrl = track.artwork_url;

            // update iframe source and load player
            iframe.src = location.protocol + "//w.soundcloud.com/player/?url=" + url +'&auto_play='+options.auto_play+''; //
            player.load(url, options);
        }

        function setViewType(type) {
            vm.viewType = type;
        }

        function handleEventsReady(event, data) {
            // handle events ready
            $log.log('handleEventsReady');
        }

        function handlePlayerReady() {
            // handle player ready
        }

        function handleFinish(event,data) {
            // handle track finished
        }

        function handleNextPageClick() {
            vm.currentPage++;
            options['auto_play'] = false;
            vm.doSearch();
        }

        function handlePrevPageClick() {
            vm.currentPage--;
            options['auto_play'] = false;
            vm.doSearch();
        }

        function handleTrackClick(track) {
            options['auto_play'] = true;
            loadTrack(track, options);
        }
    }
})();