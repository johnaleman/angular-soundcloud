(function() {
    'use strict';

    angular
        .module('app.soundcloud')
        .factory('SCFactory', SCFactory);

    SCFactory.$inject = ['$log', '$http', '$q'];

    function SCFactory($log, $http, $q) {
        var clientId = '61fb24ecaa422c90e2e87b10230a7ef4';
        var options = {
            limit: 10,
            currentPage: 1
        };

        SC.initialize({
            client_id: clientId
        });

        return {
            searchTrack: searchTrack
        };

        function getName() {
            return name;
        }

        function searchTrack(query, pOptions) {
            angular.extend(options, pOptions);

            var deferred = $q.defer();

            // find all sounds of buskers licensed under 'creative commons share alike'
            SC.get('/tracks', {
                q: query,
                limit: options.limit,
                offset: (options.currentPage - 1) * options.limit,
                linked_partitioning: 1
            }).then(function(tracks) {
                $log.log(tracks);
                deferred.resolve(tracks);
            }, function(error) {
                var msg = 'ERROR: ' + error;
                deferred.reject(msg);
            });

            return deferred.promise;
        }
    }
})();