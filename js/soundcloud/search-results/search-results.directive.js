(function() {
    'use strict';

    angular
        .module('app.soundcloud')
        .directive('searchResults', searchResults);

    function searchResults() {
        var directive = {
            templateUrl: 'js/soundcloud/search-results/search-results.html',
            restrict: 'E',
            scope: {
                trackList: '=',
                viewType: '=?',
                handleTrackClick: '&'
            },
            controller: SearchResultsController,
            controllerAs: 'vm',
            bindToController: true // because the scope is isolated
        };

        return directive;
    }

    SearchResultsController.$inject = ['$log'];

    function SearchResultsController($log) {
        var vm = this;

        vm.viewType = angular.isDefined(vm.viewType) ? vm.viewType : 'list';
        vm.handleDirTrackClick = handleDirTrackClick;

        function handleDirTrackClick(track) {
            vm.handleTrackClick()(track);
        }
    }
})();